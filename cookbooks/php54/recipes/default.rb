if !File.exist?('/etc/apt/sources.list.d/ondrej-php5-precise.list') and (node.platform_version.to_f < 12.10)
	package "python-software-properties" do
	    action :install
	end

	execute "apt-update" do
	  command "apt-get update"
	  action :nothing
	end

	execute "add-ondrejphp5-ppa" do
	  action :run
	  command "add-apt-repository ppa:ondrej/php5"
	  notifies :run, resources(:execute => "apt-update"), :immediately
	end
end