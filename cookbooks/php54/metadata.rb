name              "php54"
maintainer        "Opscode, Inc."
maintainer_email  "cookbooks@opscode.com"
license           "Apache 2.0"
description       "Installs PHP 5.4 Repo"
version           "1.1.4"

depends "build-essential"
depends "xml"
depends "mysql"

%w{ debian ubuntu }.each do |os|
  supports os
end

recipe "php54", "Installs repo"